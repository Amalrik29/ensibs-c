#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "main.h"

void saisir_personne(Personne* personne);
void afficher_personnes(Personne* tete);
void afficher_personne(Personne* personne);
Personne* ajouter_personne(Personne* tete);
Personne* supprimer_personne(Personne* tete);
void modifier_code_personne(Personne* tete);
void simuler_controle(Personne* tete);
void demander_choix(Personne* tete);
void serialiser(Personne *tete);
Personne* deserialiser();

/*
 * Initialisation des personnes.
 */
int main (int argc, char **argv) {
  Personne *lc;
  Personne *tete;
  Personne *previous;
  int nombre_personnes, i;

  /* (1) Initialisation des personnes */
  previous = NULL;
  lc = (Personne *) malloc(sizeof(Personne));
  tete = lc;

  /* (2) Demande du nombre de personnes */
  printf("Combien de personnes voulez vous créer ?\n=> ");
  scanf("%d", &nombre_personnes);

  /* (3) Création des personnes */
  for (i = 0; i<nombre_personnes-1; i++) {
    printf("Personne[%d]:\n", i);
    saisir_personne(lc);
    lc->previous_personne = previous;
    lc->next_personne = (Personne *) malloc(sizeof(Personne));
    previous = lc;
    lc = lc->next_personne;
  }

  printf("Personne[%d]:\n", nombre_personnes-1);
  saisir_personne(lc);
  lc->previous_personne = previous;
  lc->next_personne = NULL;

  /* Propose une action sur la liste à l'utilisateur */
  demander_choix(tete);
  return 0;
}

/** Permet de saisir une personnes
@param: personne, personne à saisir
*/
void saisir_personne (Personne* personne) {
  /** Saisie du nom */
  printf("\tNom :");
  scanf("%20s", personne->nom);

  /** Saisie du prénom */
  printf("\tPrénom :");
  scanf("%20s", personne->prenom);

  /** Saisie du numéro de badge */
  printf("\tNuméro de Badge :");
  scanf("%4s", personne->numero_badge);

  /** Saisie du code secret */
  printf("\tCode secret :");
  scanf("%4s", personne->code_secret);

  /** Enregistrement de la date par défaut */
  personne->date.annee = 0;
  personne->date.mois = 0;
  personne->date.jour = 0;
  personne->date.heure = 0;
}

/*
Permet d'afficher toute les personnes à partir d'une personne donnée
@param personne, personne tête de la liste à afficher
*/
void afficher_personnes (Personne* tete) {
  int n = 0;
  Personne* tmp_personne = tete;
  while (tmp_personne != NULL) {
    printf("Personne[%d]:\n", n++);
    afficher_personne(tmp_personne);
    tmp_personne = tmp_personne->next_personne;
  }
}

/*
Permet d'afficher une personne donnée
@param personne, personne à afficher
*/
void afficher_personne (Personne* personne) {
  /** Affichage du nom */
  printf("\tNom : %s\n", personne->nom);

  /** Affichage du prénom */
  printf("\tPrénom : %s\n", personne->prenom);

  /** Affichage du numéro de badge */
  printf("\tNuméro de Badge :%s\n", personne->numero_badge);

  /** Affichage du code secret */
  printf("\tCode secret : %s\n", personne->code_secret);

  /** Affichage de la dernière utilisation du badge  */
  printf("\tDernière utilisation le %02d/%02d/%04d à %02d heures\n", personne->date.jour,
                                                                     personne->date.mois,
                                                                     personne->date.annee,
                                                                     personne->date.heure);
}

/*
Permet d'ajouter une personne aprés celle donnée en paramètre
@param personne, personne qui précedera la personne à ajouter
*/
Personne* ajouter_personne (Personne* tete) {
  Personne* new_personne;
  Personne* current;

  /* Création de la nouvelle personne */
  new_personne = (Personne *) malloc(sizeof(Personne));
  saisir_personne(new_personne);

  /* Dans le cas oû il n'y a personne dans la liste */
  if (tete == NULL) {
    tete = new_personne;
    return tete;
  }

  /* On itère jusqu'à trouver une place de libre */
  current = tete;
  while (current->next_personne != NULL) current = current->next_personne;

  /* On ajoute la personne en queue de liste */
  current->next_personne = new_personne;
  new_personne->previous_personne = current;

  return tete;
}


/**
Permet de supprimer une personne de la liste
@param tete, personne à supprimer de la liste
@param badge, badge de la personne à supprimer
*/
Personne* supprimer_personne (Personne* tete) {
  Personne* previous;
  Personne* current;
  char numero_badge[5];

  printf("Veuillez entrer votre numéro de badge :\n=> ");
  scanf("%4s", numero_badge);

  /** Dans le cas où il faut supprimer la tête*/
  if (strcmp(tete->numero_badge, numero_badge) == 0) {
    previous = tete;
    tete->previous_personne = NULL;
    tete = tete->next_personne;
    free(previous);
    return tete;
  }

  /* On itère jusqu'à trouver l'élément recherché*/
  previous = NULL;
  current = tete;
  while (current->next_personne != NULL && strcmp(current->numero_badge, numero_badge) == 0) {
    previous = current;
    current = current->next_personne;
  }

  previous = current;
  current = current->next_personne;

  if (current != NULL) {
    previous->next_personne = current->next_personne;
    if (current->next_personne != NULL) {
      current->next_personne->previous_personne = previous;
    }
    free(current);
  } else {
    printf("La personne n'existe pas dans la liste !\n");
  }

  return tete;
}

void modifier_code_personne (Personne* tete) {
  Personne* current;
  char numero_badge[5];
  char code_secret[5];

  current = tete;

  printf("Veuillez saisir votre numéro de badge :\n=> ");
  scanf("%4s", numero_badge);

  current = tete;
  while (current == NULL && strcmp(current->numero_badge, numero_badge) == 1) {
    current = current->next_personne;
  }

  if (current == NULL) {
    printf("Ce numéro de badge n'existe pas !\n");
    return;
  }

  printf("Veuillez entrer vôtre code secret :\n=> ");
  scanf("%4s", code_secret);
  printf("\n");

  if (strcmp(current->code_secret, code_secret)) {
    printf("Le code secret que vous avez entré est erroné !\n");
  } else {
    printf("Veuillez entrer votre nouveau code secret :");
    scanf("%4s", current->code_secret);
  }
}

/*
Permet de simuler un controle de badge
@param tete, personne de tete
*/
void simuler_controle (Personne* tete) {
  Personne* current;
  char numero_badge[5];
  char code_secret[5];
  time_t time_raw_format;
  struct tm * ptr_time;

  time(&time_raw_format);
  ptr_time = localtime(&time_raw_format);

  current = tete;

  printf("Veuillez entrer vôtre numéro de badge :\n=> ");
  scanf("%4s", numero_badge);

  while (current == NULL && strcmp(current->numero_badge, numero_badge) == 1) {
    current = current->next_personne;
  }

  if (current == NULL) {
    printf("Ce numéro de badge n'existe pas !\n");
    return;
  }

  printf("Veuillez entrer vôtre code secret :\n=> ");
  scanf("%4s", code_secret);
  printf("\n");

  if (strcmp(current->code_secret, code_secret)) {
    printf("Le code secret que vous avez entré est erroné !\n");
  } else {
    printf("Bienvenu(e) %s %s !\n", current->prenom, current->nom);
    /** On met à jour la date de dernière authentification réussie*/
    current->date.annee = ptr_time->tm_year + 1900;
    current->date.mois  = ptr_time->tm_mon + 1;
    current->date.jour  = ptr_time->tm_mday;
    current->date.heure = ptr_time->tm_hour;
  }
}

/*
Permet de serialiser une liste doublement chainé dans un fichier donné
@param tete, tete de la liste
*/
void serialiser (Personne *tete) {
  FILE* fichier;
  Personne* current;
  char path[251];

  printf("Dans quel fichier voulez vous sauvegarder vos données ?\n=> ");
  scanf("%250s", path);

  fichier = fopen(path, "w");

  if (fichier != NULL) {
    current = tete;
    while (current != NULL) {
      fprintf(fichier, "%s %s %s %s %02d%02d%04d%02d", current->nom,
                                                                  current->prenom,
                                                                  current->numero_badge,
                                                                  current->code_secret,
                                                                  current->date.jour,
                                                                  current->date.mois,
                                                                  current->date.annee,
                                                                  current->date.heure);
      current = current->next_personne;
    }
    printf("Sauvegarde effectuée dans %s",path);
    fclose(fichier);
  } else {
    printf("Le fichier %s n'existe pas !",path);
  }
}

/*
Permet de récupérer une liste chainée à partir d'un fichier de sauvegarde
@return Personne*, tete de la liste de personne
*/
Personne* deserialiser() {
  FILE* fichier;
  Personne* tete = malloc(sizeof(Personne));
  Personne* current;
  Personne* previous;
  char path[251];
  int ret;

  printf("Depuis quel fichier voulez vous charger votre liste de personne ?\n=> ");
  scanf("%250s", path);

  fichier = fopen(path, "r");

  if (fichier !=NULL) {
    current = tete;
    previous = NULL;
    while ((ret = fscanf(fichier, "%s %s %s %s %02d%02d%04d%02d", current->nom,
                                                                      current->prenom,
                                                                      current->numero_badge,
                                                                      current->code_secret,
                                                                      &current->date.jour,
                                                                      &current->date.mois,
                                                                      &current->date.annee,
                                                                      &current->date.heure)) != EOF && ret != 0) {
      current->previous_personne = previous;
      if (previous != NULL) {
        previous->next_personne = current;
      }
      previous = current;
      current = malloc(sizeof(Personne));
    }
    free(current);
    fclose(fichier);
    return tete;
  } else {
    printf("Le fichier que vous avez tenté d'ouvrir n'existe pas !\n");
  }
  return NULL;
}

/*
Permet de simuler les opérations sur une liste
@param tete, tete de la liste
*/
void demander_choix (Personne* tete) {
  Personne* tmp;
  int choix;
  do {
    printf("1 - Afficher toutes les personnes\n");
    printf("2 - Ajouter une personne\n");
    printf("3 - Supprimer une personne\n");
    printf("4 - Modifier le code d'une personne\n");
    printf("5 - Simuler le contrôle d'accés\n");
    printf("6 - Sauvegarder la liste de personnes\n");
    printf("7 - Charger une liste chainée\n");
    printf("0 - Quitter le programme\n");
    printf("Votre choix :\n=> ");
    scanf("%d", &choix);

    switch (choix) {
      case 1:
        afficher_personnes(tete);
        break;
      case 2:
        tete = ajouter_personne(tete);
        break;
      case 3:
        tete = supprimer_personne(tete);
        break;
      case 4:
        modifier_code_personne(tete);
        break;
      case 5:
        simuler_controle(tete);
        break;
      case 6:
        serialiser(tete);
        break;
      case 7:
        tmp = deserialiser();
        if(tmp != NULL)
        {
          tete = tmp;
        }
        break;
      case 0:
        break;
      default:
        printf("Ce choix n'existe pas !\n");
    }
  } while(choix != 0);
}
