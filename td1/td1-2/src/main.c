/**
 * \file main.h
 * \brief Operator priority.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \author Romain KRAFT <romain.kraft@protonmail.com>
 * \version 0.1-dev
 * \date 10 January 2018
 */

#include <stdlib.h>
#include <stdio.h>

#include "main.h"

/**
 * Make some arithmetic operation.
 */
int main(int argc, char *argv[]) {
    int x_value;

    /*
     * Priorité de la multiplication.
     */
    x_value = -3 + 4 * 5 - 6;

    printf("X = -3 + 4 * 5 - 6\t// X = %d\n", x_value);

    /*
     * Priorité des parenthèses.
     */
    x_value = (-3 + 4) * 5 - 6;

    printf("X = (-3 + 4) * 5 - 6\t// X = %d\n", x_value);

    /*
     * Priorité des parenthèses.
     */
    x_value = -3 + (4 * 5) - 6;

    printf("X = -3 + (4 * 5) - 6\t// X = %d\n", x_value);

    /*
     * Priorité des parenthèses.
     */
    x_value = -3 + 4 * (5 - 6);

    printf("X = -3 + 4 * (5 - 6)\t// X = %d\n", x_value);

    return 0;
}
