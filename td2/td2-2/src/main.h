/**
 * \file main.h
 * \brief Hello world.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \author Romain KRAFT <romain.kraft@protonmail.com>
 * \version 0.1-dev
 * \date 10 January 2018
 */

#ifndef __MAIN_H
#define __MAIN_H

#define MAX_BUF_LENGTH 15  /* Maximum buffer length for user input. */

void bubble_sort(void);
void successive_minimal_sort(void);
void insertion_sort(void);
int* rand_table(const int length);
void print_table(int * tab, const int size);

#endif /* __MAIN_H */
