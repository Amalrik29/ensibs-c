/**
 * \file main.h
 * \brief Figures.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \author Romain KRAFT <romain.kraft@protonmail.com>
 * \version 0.1-dev
 * \date 10 January 2018
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "main.h"

/**
 * Draw figures to stdout.
 */
int main(int argc, char *argv[]) {
    char buf[MAX_BUF_LENGTH], continue_;

    do {
        draw_menu();
        continue_ = fget_char("Continue? (y/n)> ", buf, 2);
    } while (continue_ == 'y');

    return 0;
}

void figure_1(int n) {
    int i, j;

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            printf("*");
        }

        printf("\n");
    }
}

void figure_2(int n) {
    int i, j;

    for (i = 0; i < n; i++) {
        for (j = 0; j < i; j++) {
            printf("*");
        }
        printf("\n");
    }

    for (i = n; i > 0; i--) {
        for (j = 0; j < i; j++) {
            printf("*");
        }
        printf("\n");
    }
}

void figure_3(int n) {
    int i, j;

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            if (j < n-i){
                printf(" ");
            } else {
                printf("*");
            }
        }

        printf("\n");
    }

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
            if (j < i) {
                printf(" ");
            } else {
                printf("*");
            }
        }

        printf("\n");
    }
}

void figure_4(int n) {
    int i, j, k;

    for (i = 0; i <= n; i++) {
        for (k = 0; k < n-i; k++) {
            printf(" ");
        }

        for (j = 0; j < 2*i-1; j++) {
            printf("*");
        }

        printf("\n");
    }
}

void figure_5(int n) {
    int i, j, k;

    for (i = 0; i <= n; i++) {
        for (k = 0; k < n-i; k++) {
            printf(" ");
        }
        for (j = 0; j < 2*i-1; j++) {
            printf("*");
        }
        printf("\n");
    }

    for (i = n-1; i > 0; i--) {
        for (k=0; k < n-i; k++) {
            printf(" ");
        }

        for (j = 0; j < 2*i-1; j++) {
            printf("*");
        }

        printf("\n");
    }
}

void draw_menu () {
    int choice, size;
    char buf[MAX_BUF_LENGTH];
    choice = fget_int("Figure? (1-5)> ", buf, 2);

    do {
        size = fget_int("Size? (1-20)> ", buf, sizeof(MAX_BUF_LENGTH));

        if (size < 1 || size > 20) {
            printf("Please enter a valid size (1-20)!\n");
        }
    } while (size < 1 || size > 20);

    switch (choice) {
        case 1:
            figure_1(size);
            break;
        case 2:
            figure_2(size);
            break;
        case 3:
            figure_3(size);
            break;
        case 4:
            figure_4(size);
            break;
        case 5:
            figure_5(size);
            break;
        default:
            printf("Please enter a valid figure id (1-5)!\n");
            exit(1);
    }
}

/** Get integer from standard input.
 *
 * \param message      Message to display to the user for input.
 * \param buf          Buffer used to store user input.
 * \param max_buf_size Maximum buffer size used to store user input.
 *
 * \return integer Integer number retrieved from user input.
 */
int fget_int(const char message[], char *buf, const int max_buf_size) {
    int integer, finished = 0;
    char junk_char;

    do {
        printf("%s", message);

        /* Fill the buffer with user input and then convert the buffer to integer. */
        if (fgets(buf, max_buf_size, stdin) != NULL) {
            /*
            printf("buf = {");
            for (int i = 0; i < sizeof(buf) - 1; i++) {
                printf("'\\x%x', ", buf[i]);
            }
            printf("'\\%x'};\n", buf[sizeof(buf) - 1]);
            */

            if (sscanf(buf, "%d", &integer) != 1) {  /* Buffer can't be converted to integer. */
                fprintf(stderr, "Please enter a valid integer!\n");
            } else {
                finished = 1;
            }
        }

        /** Eventually flush the input buffer in order to prevent overflow issues.
         *
         * __Case 1:__ Line break is inside the buffer, e.g. `max_buf_size = 5` and `stdin = '123\n'`:
         *
         * ```c
         * char buf[] = {'1', '2', '3', '\n', '\0'};          // ==> {\x31, \x32, \x33, \xa, \0}
         *                                                    //     ^-[&buf]                  ^-[&buf + max_buf_size]
         *                                                    //     ^----------[buf]----------^
         *                                                    //
         *                                                    // --> strlen(buf) = 4
         *
         * char buf2[] = {'\x0', '\x0', '\x0', '\x0', '\0'};  // ==> {\x0, \x0, \x0, \x0, \0}
         *                                                    //     ^-[&buf2]              ^-[&buf2 + max_buf_size]
         *                                                    //     ^--------[buf2]--------^
         *                                                    //
         *                                                    // --> strlen(buf2) = 0
         * ```
         *
         * __Case 2:__ Line break is outside the buffer leading to overflow on next buffer, e.g. `max_buf_size = 5` and `stdin = '1234\n'`:
         *
         * ```c
         * char buf[] = {'1', '2', '3', '4', '\0'};           // ==> {\x31, \x32, \x33, \x34, \0}
         *                                                    //     ^-[&buf]                   ^-[&buf + max_buf_size]
         *                                                    //     ^-----------[buf]----------^
         *                                                    //
         *                                                    // --> strlen(buf) = 4
         *
         * char buf2[] = {'\n', '\x0', '\x0', '\x0', '\0'};   // ==> {\xa, \x0, \x0, \x0, \0}
         *                                                    //     ^-[&buf2]              ^-[&buf2 + max_buf_size]
         *                                                    //     ^--------[buf2]--------^
         *                                                    //
         *                                                    // --> strlen(buf2) = 1
         * ```
         */
        if (buf[strlen(buf) - 1] != 0xa) {  /* Use strlen as index to handle non-full buffer. */
            while ((junk_char = getchar()) != 0xa && junk_char != EOF) {
                /* printf("Flushing! Removing 0x%x from input.\n", junk_char); */
            }
        }
    } while (!finished);

    return integer;
}

/** Get char from standard input.
 *
 * \param message      Message to display to the user for input.
 * \param buf          Buffer used to store user input.
 * \param max_buf_size Maximum buffer size used to store user input.
 *
 * \return char_ Char retrieved from user input.
 */
char fget_char(const char message[], char *buf, const int max_buf_size) {
    int finished = 0;
    char char_;
    char junk_char;

    do {
        printf("%s", message);

        /* Fill the buffer with user input and then convert the buffer to char. */
        if (fgets(buf, max_buf_size, stdin) != NULL) {
            /*
            printf("buf = {");
            for (int i = 0; i < sizeof(buf) - 1; i++) {
                printf("'\\x%x', ", buf[i]);
            }
            printf("'\\%x'};\n", buf[sizeof(buf) - 1]);
            */

            if (sscanf(buf, "%c", &char_) != 1) {  /* Buffer can't be converted to char. */
                fprintf(stderr, "Please enter a valid char!\n");
            } else {
                finished = 1;
            }
        }

        /** Eventually flush the input buffer in order to prevent overflow issues.
         *
         * __Case 1:__ Line break is inside the buffer, e.g. `max_buf_size = 5` and `stdin = '123\n'`:
         *
         * ```c
         * char buf[] = {'1', '2', '3', '\n', '\0'};          // ==> {\x31, \x32, \x33, \xa, \0}
         *                                                    //     ^-[&buf]                  ^-[&buf + max_buf_size]
         *                                                    //     ^----------[buf]----------^
         *                                                    //
         *                                                    // --> strlen(buf) = 4
         *
         * char buf2[] = {'\x0', '\x0', '\x0', '\x0', '\0'};  // ==> {\x0, \x0, \x0, \x0, \0}
         *                                                    //     ^-[&buf2]              ^-[&buf2 + max_buf_size]
         *                                                    //     ^--------[buf2]--------^
         *                                                    //
         *                                                    // --> strlen(buf2) = 0
         * ```
         *
         * __Case 2:__ Line break is outside the buffer leading to overflow on next buffer, e.g. `max_buf_size = 5` and `stdin = '1234\n'`:
         *
         * ```c
         * char buf[] = {'1', '2', '3', '4', '\0'};           // ==> {\x31, \x32, \x33, \x34, \0}
         *                                                    //     ^-[&buf]                   ^-[&buf + max_buf_size]
         *                                                    //     ^-----------[buf]----------^
         *                                                    //
         *                                                    // --> strlen(buf) = 4
         *
         * char buf2[] = {'\n', '\x0', '\x0', '\x0', '\0'};   // ==> {\xa, \x0, \x0, \x0, \0}
         *                                                    //     ^-[&buf2]              ^-[&buf2 + max_buf_size]
         *                                                    //     ^--------[buf2]--------^
         *                                                    //
         *                                                    // --> strlen(buf2) = 1
         * ```
         */
        if (buf[strlen(buf) - 1] != 0xa) {  /* Use strlen as index to handle non-full buffer. */
            while ((junk_char = getchar()) != 0xa && junk_char != EOF) {
                /* printf("Flushing! Removing 0x%x from input.\n", junk_char); */
            }
        }
    } while (!finished);

    return char_;
}
