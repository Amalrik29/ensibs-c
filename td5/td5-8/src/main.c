/*
 * TP 5 : Liste chaînées
 *
 * +------+--------------+        +------+--------------+        +------+--------------+
 * |      |              |        |      |              |        |      |              |        
 * |  0   |      +--------------->|  1   |      +--------------->|  2   |      +---------------/
 * |      |              |        |      |              |        |      |              |      
 * +------+--------------+        +------+--------------+        +------+--------------+
 *
 */

#include <stdio.h>
#include <stdlib.h>

#define N 3

typedef struct maillon {
    int x;
    struct maillon * suiv;
} maillon;

int main(int argc, char *argv[]) {
    maillon *lc;
    maillon *tete;
    int cpt;

    /* (1) Initialisation des maillons */
    lc = (maillon *) malloc(sizeof(maillon));
    tete = lc;

    /* (2) Creation des maillons en fin de liste */
    for(cpt=1; cpt<N; cpt++) { /* Pour tous les maillons à créer */
        lc->suiv = (maillon *) malloc(sizeof(maillon));
        lc = lc->suiv;
    }

    lc->suiv = NULL;

    cpt = 0;
    lc = tete;

    /* (3) Remplissage des valeurs de chacun des maillons */
    while (lc != NULL) { /* Tant que le maillon courant n'est pas le suivant du dernier maillon de la liste */
        lc->x = cpt; /* Affectation */
        cpt++; /* Increment du compteur */
        lc = lc->suiv; /* Passe au suivant */
    }

    lc = tete;

    while (lc != NULL) { /* Tant que le maillon courant n'est pas le suivant du dernier maillon de la liste */
        printf("Valeur du champs courant = %d\n", lc->x);
        printf("Adresse maillon courant= %X et du suivant %X\n", lc, lc->suiv);
        lc = lc->suiv; /* Passe au suivant */
    }

    return 0;
}