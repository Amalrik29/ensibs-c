 /*
 * TP 5 : Pointeurs
 *
 * lecture de l'adresse d'une variable
 */

#include <stdio.h>

int main(int argc, char *argv[]) {
    int i, j;
    i = 5;
    j = 12;

    printf("La variable i est située à l'adresse 0x%x et vaut %d\n", &i, i);
    printf("La variable j est située à l'adresse 0x%x et vaut %d\n", &j, j);

    return 0;
}